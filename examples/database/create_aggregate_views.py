#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-

"""Example of aggregate views creation.
"""

# Built-in imports
from pkg_resources import resource_filename  # Get absolute path to local configuration files

# Third party imports
from psycopg2 import Error as psyError

# Local imports
from lab_utils.database import Database
from custom_logging import configure_logging, getLogger

# Read logging setup from configuration file and create the logger
logger = None
try:
    configure_logging(config_file=resource_filename(__name__, 'conf/logging.ini'),
                      logger_name='agg_view')
    logger = getLogger()
except RuntimeError as e:
    print("{}: {}".format(type(e).__name__, e))
    exit(1)

try:
    # Welcome code
    logger.info("Welcome to the Python-utils database management module")
    logger.info("This is an example showing how to create an Aggregate Views for a TimescaleDB table.")

    # Show some basic usage of the module
    db = Database(config_file=resource_filename(__name__, 'conf/database.ini'))
    db.connect()

    db.create_aggregate_view(
        table_name='test'
    )

    db.close()

except (TypeError, psyError, ValueError) as e:
    logger.error("{}: {}".format(type(e).__name__, e))

except BaseException as e:
    logger.exception("{}: {}".format(type(e).__name__, e))

else:
    exit(0)

exit(1)
