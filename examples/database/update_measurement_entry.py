#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-

""" Updates a measurement entry.
"""

# Built-in imports
from pkg_resources import resource_filename  # Get absolute path to local configuration files

# Third party imports
from psycopg2 import (
    Error as psyError
)

# Local imports
from lab_utils.database import Database
from lab_utils.custom_logging import configure_logging, getLogger

# Read logging setup from configuration file and create the logger
logger = None
try:
    configure_logging(resource_filename(__name__, 'conf/logging.ini'))
    logger = getLogger('next_serial_id')
except RuntimeError as e:
    print("{}: {}".format(type(e).__name__, e))
    exit(1)

try:
    # Welcome code
    logger.info("Welcome to the Python-utils database management module")
    logger.info("This is an example showing how to update a measurement entry")

    # Show some basic usage of the module
    db = Database(config_file=resource_filename(__name__, 'conf/database.ini'))
    db.connect()

    # Two update methods
    db.update_measurement(
        table_name='test',
        measurement_id=1,
        columns=['number', 'text'],
        values=[3, 'number should be 3']
    )

    db.update_measurement(
        table_name='test',
        measurement_id=2,
        pairs=[
            ['number', -9],
            ['text', 'number is now -9']
        ],
    )

    # Close
    db.close()

except (TypeError, psyError, ValueError) as e:
    logger.error("{}: {}".format(type(e).__name__, e))
    exit(1)

except BaseException as e:
    logger.exception("{}: {}".format(type(e).__name__, e))
    exit(1)

exit(0)
