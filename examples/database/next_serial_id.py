#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-

""" Get the next free serial ID.
"""

# Built-in imports
from pkg_resources import resource_filename  # Get absolute path to local configuration files

# Third party imports
from psycopg2 import (
    Error as psyError
)

# Local imports
from lab_utils.database import Database
from lab_utils.custom_logging import configure_logging, getLogger

# Read logging setup from configuration file and create the logger
logger = None
try:
    configure_logging(resource_filename(__name__, 'conf/logging.ini'))
    logger = getLogger('next_serial_id')
except RuntimeError as e:
    print("{}: {}".format(type(e).__name__, e))
    exit(1)

try:
    # Welcome code
    logger.info("Welcome to the Python-utils database management module")
    logger.info("This is an example showing how to fetch the next free ID in a serialized table")

    # Show some basic usage of the module
    db = Database(config_file=resource_filename(__name__, 'conf/database.ini'))
    db.connect()

    # Is the table empty?
    for table in ['test', 'dbs']:
        getLogger().info('Is table {} empty? {}'.format(table, db.check_empty_table(table)))
        getLogger().info('Next serial ID: {}'.format(db.fetch_next_serial_id(table)))

    # Close
    db.close()

except (TypeError, psyError, ValueError) as e:
    logger.error("{}: {}".format(type(e).__name__, e))
    exit(1)

except BaseException as e:
    logger.exception("{}: {}".format(type(e).__name__, e))
    exit(1)

exit(0)
