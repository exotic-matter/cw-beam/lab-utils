#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-

"""Example of database creation.
"""

# Built-in imports
from pkg_resources import resource_filename  # Get absolute path to local configuration files

# Third party imports
from psycopg2 import Error as psyError

# Local imports
from lab_utils.database import Database
from custom_logging import configure_logging, getLogger

# Read logging setup from configuration file and create the logger
logger = None
try:
    configure_logging(config_file=resource_filename(__name__, 'conf/logging.ini'))
    logger = getLogger('create_database')
except BaseException as e:
    print("{}: {}".format(type(e).__name__, e))
    exit(1)

try:
    # Welcome code
    logger.info("Welcome to the Python-utils database management module")
    logger.info("This is an example showing how to create a database with a TimescaleDB extension.")

    # Connect to the parent database 'postgres'
    db = Database(
        config_file=resource_filename(__name__, 'conf/database.ini'),
        database='postgres',
        user='postgres',
        passfile='~/.pgpass_admin'
    )
    db.connect()

    # Create database
    db.create_database(
        db_name='beam',
        owner='cw-beam',
    )

    db.close()

except (TypeError, psyError, ValueError) as e:
    logger.error("{}: {}".format(type(e).__name__, e))
    exit(1)

except BaseException as e:
    logger.exception("{}: {}".format(type(e).__name__, e))
    exit(1)

exit(0)
