#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-

"""Example of data insertion.

Based on `this tutorial <https://www.postgresqltutorial.com/postgresql-insert/>`_.
"""

# Built-in imports
from pkg_resources import resource_filename # Get absolute path to local configuration files

# Third party imports
from psycopg2 import (
    Error as psyError,
    IntegrityError
)

# Local imports
from lab_utils.database import Database
from custom_logging import configure_logging, getLogger

# Read logging setup from configuration file and create the logger
logger = None
try:
    configure_logging(resource_filename(__name__, 'conf/logging.ini'))
    logger = getLogger('insert_data')
except RuntimeError as e:
    print("{}: {}".format(type(e).__name__, e))
    exit(1)

try:
    # Welcome code
    logger.info("Welcome to the Python-utils database management module")
    logger.info("This is an example showing how to insert data into an existing table")

    # Show some basic usage of the module
    db = Database(config_file=resource_filename(__name__, 'conf/database.ini'))
    db.connect()

    # A valid entry from 2016 and with column 'an_int_column' missing
    column_list = [
        'time',
        'a_double_column',
        'a_text_column',
    ]

    data_list = [
        '2016-06-22 19:10:25-07',
        -7.2,
        'a valid entry from 2016',
    ]

    db.new_entry(
        table_name='test',
        columns=column_list,
        data=data_list,
        check_columns=True
    )

    # A valid entry from NOW(), the default 'time' value, and the value Nan
    column_list = [
        'an_int_column',
        'a_double_column',
        'a_text_column',
    ]

    data_list = [
        8,
        'Nan',
        'a valid entry from NOW()',
    ]
    db.new_entry(
        table_name='test',
        columns=column_list,
        data=data_list,
        check_columns=True
    )

    # An invalid entry, column 'an_int_column' must be positive
    # Raises exception psycopg2.CheckViolation
    column_list = [
        'an_int_column',
        'a_double_column',
        'a_text_column',
    ]

    data_list = [
        -3,
        4.67,
        'a valid entry from 2016',
    ]

    try:
        db.new_entry(
            table_name='test',
            columns=column_list,
            data=data_list,
            check_columns=True
        )
    except IntegrityError as e:
        logger.info('As expected, an \'IntegrityError\' was raised')
        logger.info("{}: {}".format(type(e).__name__, e))

    db.close()

except (TypeError, psyError, ValueError) as e:
    logger.error("{}: {}".format(type(e).__name__, e))
    exit(1)

except BaseException as e:
    logger.exception("{}: {}".format(type(e).__name__, e))
    exit(1)

exit(0)
