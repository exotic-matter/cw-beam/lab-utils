#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-

"""Example of column creation.

Based on `this tutorial <https://www.postgresqltutorial.com/postgresql-add-column/>`_.
"""

# Built-in imports
from pkg_resources import resource_filename # Get absolute path to local configuration files

# Third party imports
from psycopg2 import Error as psyError

# Local imports
from lab_utils.database import Database, DataType, Constraint
from custom_logging import configure_logging, getLogger

# Read logging setup from configuration file and create the logger
logger = None
try:
    configure_logging(resource_filename(__name__, 'conf/logging.ini'))
    logger = getLogger('insert_data')
except RuntimeError as e:
    print("{}: {}".format(type(e).__name__, e))
    exit(1)

try:
    # Welcome code
    logger.info("Welcome to the Python-utils database management module")
    logger.info("This is an example showing how to create some new columns")

    # Show some basic usage of the module
    db = Database(config_file=resource_filename(__name__, 'conf/database.ini'))
    db.connect()

    db.new_column(
        table_name='test',
        column_name='an_int_column',
        data_type=DataType.int,
        constraints=[Constraint.positive],
    )

    db.new_column(
        table_name='test',
        column_name='a_double_column',
        data_type=DataType.double,
        constraints=None,
    )

    db.new_column(
        table_name='test',
        column_name='a_text_column',
        data_type=DataType.string,
    )

    db.close()

except (TypeError, psyError, ValueError) as e:
    logger.error("{}: {}".format(type(e).__name__, e))
    exit(1)

except BaseException as e:
    logger.exception("{}: {}".format(type(e).__name__, e))
    exit(1)

exit(0)
