#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-

"""Example of data fetching.
"""

# Built-in imports
from pkg_resources import resource_filename # Get absolute path to local configuration files

# Third party imports
from psycopg2 import (
    Error as psyError
)

# Local imports
from lab_utils.database import Database
from lab_utils.custom_logging import configure_logging, getLogger

# Read logging setup from configuration file and create the logger
logger = None
try:
    configure_logging(resource_filename(__name__, 'conf/logging.ini'))
    logger = getLogger('fetch_data')
except RuntimeError as e:
    print("{}: {}".format(type(e).__name__, e))
    exit(1)

try:
    # Welcome code
    logger.info("Welcome to the Python-utils database management module")
    logger.info("This is an example showing how to fetch the latest data from a time-ordered table")

    # Show some basic usage of the module
    db = Database(config_file=resource_filename(__name__, 'conf/database.ini'))
    db.connect()

    # All values from table 'pressure'
    pressures = db.fetch_latest_value(
        table_name='pressure'
    )
    logger.info('Latest pressures: {}'.format(pressures))

    # Fetch latest source and shield temperatures
    temperatures = db.fetch_latest_value(
        table_name='cryosystem',
        column_name=['source_temp', 'shield_temp']
    )
    logger.info('Latest temperatures: {}'.format(temperatures))

    # Errors! Invalid table or column
    try:
        db.fetch_latest_value(table_name='invalid')
    except psyError as e:
        logger.error("Exception <{}> caught: {}".format(type(e).__name__, e))

    try:
        db.fetch_latest_value(
            table_name='pressure',
            column_name=['invalid']
        )
    except psyError as e:
        logger.error("Exception <{}> caught: {}".format(type(e).__name__, e))

    db.close()

except (TypeError, psyError, ValueError) as e:
    logger.error("{}: {}".format(type(e).__name__, e))
    exit(1)

except BaseException as e:
    logger.exception("{}: {}".format(type(e).__name__, e))
    exit(1)

exit(0)
