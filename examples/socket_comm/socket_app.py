#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-

"""Example of a daemon listening on a TCP
socket. Send 'quit' to terminate the server.
Any other message will receive a help string
as a reply.
"""

from lab_utils.custom_logging import configure_logging, getLogger
from lab_utils.socket_comm import Server
from zc.lockfile import LockError

# Read logging setup from configuration file and create the logger
logger = None
try:
    configure_logging()
    logger = getLogger('socket_app')
except RuntimeError as e:
    print("{}: {}".format(type(e).__name__, e))
    exit(1)

# Start the server
try:
    logger.info('Creating Server object')
    s = Server(pid_file_name='/tmp/socket_app.pid')
    logger.info('Starting server')
    s.start_daemon()
except LockError:
    logger.error('Daemon is probably running elsewhere')
except OSError:
    logger.error('Socket error')
else:
    logger.info('Quitting')
    exit(0)

exit(1)


