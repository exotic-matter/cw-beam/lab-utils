#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-

"""Example of a TCP socket client. The client
loads a configuration file and sends a message
to the specified address. The message is the
arguments provided to the script, or 'quit'
if no argument is provided.
"""

# Imports
import sys
from lab_utils.socket_comm import Client
from configparser import Error as ConfigError
from lab_utils.__project__ import __documentation__ as docs_url

c = None
try:
    c = Client()

    msg = 'quit'
    if len(sys.argv) > 1:
        msg = sys.argv[1]
    reply = c.send_message(msg)
    print(reply)

# Error management
except ConfigError as e:
    print('Error with configuration file: ', e)
    print('Check out the package documentation for more information:\n{}'.format(docs_url))

except OSError as e:
    print('Error! Connection could not be established.')
    print('Are you sure a server is running on < {h}:{p} > ?'.format(
        h=c.host,
        p=c.port
    ))

else:
    exit(0)

exit(1)

