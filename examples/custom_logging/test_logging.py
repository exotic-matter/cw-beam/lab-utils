#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-

"""Example of logging using the
:obj:`lab_utils.custom_logging` module.
"""

from custom_logging import configure_logging, getLogger

# Read logging setup from configuration file and create the logger
logger = None
try:
    configure_logging()
    logger = getLogger('test_logging')
except RuntimeError as e:
    print("{}: {}".format(type(e).__name__, e))
    exit(1)

# Log some messages
logger.debug('A debug message')
logger.info('An info message')
logger.success('A success message')
logger.warning('A warning message')
logger.error('An error message')
logger.critical('A critical message')
logger.exception('An exception message')
