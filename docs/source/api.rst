API Reference
=============

.. rubric:: Description
.. automodule:: lab_utils
.. currentmodule:: lab_utils

.. rubric:: Modules
.. autosummary::
    :toctree: api

    database <lab_utils.database>
    socket_comm <lab_utils.socket_comm>
    custom_logging <lab_utils.custom_logging>