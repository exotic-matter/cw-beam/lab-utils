Examples
========

.. currentmodule:: lab_utils

This documentation is intended to show practical usage examples of the different modules included in the
:obj:`lab_utils` package.

.. _configuration-files:

Configuration files
-------------------

The :obj:`config` method of each module expects a configuration file with a specific pattern. In addition, a sample
file accepted by the standard :obj:`logging.config` method is also provided.

.. toctree::
    :maxdepth: 1

    Logging <examples/conf/conf_logging>
    Database <examples/conf/conf_database>


Database
--------

.. toctree::
    :maxdepth: 2

    examples/ex_database