Welcome to Lab Utils!
=====================

.. image:: https://badge.fury.io/py/lab-utils.svg
    :target: https://badge.fury.io/py/lab-utils
    :alt: PyPI Latest Version

.. image:: https://gitlab.ethz.ch/exotic-matter/cw-beam/lab-utils/badges/master/pipeline.svg
    :target: https://gitlab.ethz.ch/exotic-matter/cw-beam/lab-utils/-/commits/master
    :alt: Pipeline Status

.. image:: https://gitlab.ethz.ch/exotic-matter/cw-beam/lab-utils/badges/master/coverage.svg
    :target: https://gitlab.ethz.ch/exotic-matter/cw-beam/lab-utils/-/commits/master
    :alt: Coverage Report

.. image:: https://readthedocs.org/projects/lab-utils/badge/?version=stable
    :target: https://lab-utils.readthedocs.io/en/latest/?badge=stable
    :alt: Documentation Status

.. image:: https://img.shields.io/badge/License-GPLv3-blue.svg
    :target: https://www.gnu.org/licenses/gpl-3.0
    :alt: License: GPL v3


:obj:`lab_utils` is a collection of useful modules for Python development in the context of scientific laboratory
work. It was created to standardize common tasks and features used by many different apps and users. The package aims to
provide simple, efficient and robust protocols in the following areas:

 -  **PostgreSQL Database Interface**. The :obj:`database<lab_utils.database>` module provides a simple interface to
    manage connections to a `PostgreSQL <https://www.postgresql.org/>`_ database. It uses the package :obj:`psycopg2` (a
    Python wrapper of the C library `libpq <https://www.postgresql.org/docs/11/libpq.html>`_) to provide simple database
    functionality.

 -  **Daemon-like TCP Server**. The :obj:`socket_comm<lab_utils.socket_comm>` module provides a daemon-like TCP server
    base class. The :class:`lab_utils.socket_comm.Server` is supposed to start and run indefinitely in the background,
    while listening for incoming communications over a TCP port. When a message is received, a string parser calls
    the appropriate method. Users should develop their own child class inheriting from :class:`lab_utils.socket_comm.Server`
    and implementing the necessary methods for their particular needs.

    The module also provides a simple :class:`lab_utils.socket_comm.Client` to send messages to a running
    :class:`lab_utils.socket_comm.Server` and receive the reply.

If this is your first time using :obj:`lab_utils`, have a look at our :doc:`Readme<project/README>` for a more
detailed summary and installation instructions. If you're already familiar with this package, or you want to dive
straight in, you can jump to the :doc:`API reference <api>`. There are also some :doc:`examples<examples>` which
demonstrate specific applications of the modules.




.. toctree::
    :maxdepth: 1

    Welcome <self>
    Readme <project/README.md>
    api
    examples
    Changelog <project/CHANGELOG.md>
    Contributing <project/CONTRIBUTING.md>
    License <project/LICENSE.md>
