.. currentmodule:: lab_utils

Installing
==========
:obj:`lab_utils` can be obtained from pip via

.. code-block:: bash

   pip install lab_utils

You can also get :obj:`lab_utils` from its current source on GitHub, to
get all the latest and greatest features. :obj:`lab_utils` is under
active development, and many new features are being added. However, note
that the API is currently unstable at this time.

.. code-block:: bash

   git clone https://github.com/mrocklin/sparse.git
   cd ./sparse/
   pip install .